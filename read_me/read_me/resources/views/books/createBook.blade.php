@extends('layouts.plantilla')
@section('title','show books')
@section('page','Crear libro')

@section('content')
    <form action="{{route('books.add')}}" method="POST" style="margin-left: 30%;margin-right: 30%;text-align: center" enctype="multipart/form-data">
        @csrf
        <div >
            <label for="floatingInput">Titulo</label>
            <input type="text" class="form-control" id="tittle" placeholder="Titulo" name="name" value="{{old('name')}}">
            @error('name')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <label for="floatingInput">Autor</label>
            <input type="text" class="form-control" id="author" placeholder="Miguel Cervantes" value="{{old('author')}}" name="author">
            @error('author')
            <small class="alert-danger">{{$message}}</small>
        @enderror
        </div>
        <div>
            <p for="floatingInput">Portada</p>
            <input type="file" name="cover" accept="image/*">
            @error('cover')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <p for="floatingInput">Libro en PDF</p>
            <input type="file" name="pdf" accept="application/pdf">
            @error('pdf')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <label for="floatingInput">Genero</label>
            <input type="text" class="form-control" id="genre" placeholder="Historia-Terror-Ciencia" name="genre" value="{{old('genre')}}">
            @error('genre')
            <small class="alert-danger">{{$message}}</small>
        @enderror
        </div>
        <div>
            <div><label for="floatingInput">Resumen</label></div>
            <textarea class="form-control" placeholder="Escribe una reseña del libro" id="summary" name="summary" style="height: 100px">{{old('summary')}}</textarea>
            @error('summary')
            <small class="alert-danger">{{$message}}</small>
        @enderror
        </div>
      </div>
      <button class="btn btn-primary mt-3" type="submit">submit</button>
    </form>
@endsection