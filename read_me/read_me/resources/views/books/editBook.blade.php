@extends('layouts.plantilla')
@section('title','show books')
@section('page','Editar libro')

@section('content')
    <form action="{{route('books.update',$book)}}" method="POST" style="margin-left: 30%;margin-right: 30%;text-align: center">
        @csrf
        @method('put')
        <div >
            <label for="floatingInput">Titulo</label>
            <input type="text" class="form-control" id="tittle" placeholder="Titulo" name="name" value="{{old('name',$book->bookname)}}">
            @error('name')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <label for="floatingInput">Autor</label>
            <input type="text" class="form-control" id="author" placeholder="Miguel Cervantes" name="author" value="{{old('author',$book->author)}}">
            @error('author')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <label for="floatingInput">Portada</label>
            <img style="margin-left: 35%; border-radius: 50%" src="{{$book->cover}}" alt="Mi imagen" id="miImagen" height="150" width="150">
            
        </div>
        <div>
            <label for="floatingInput">Genero</label>
            <input type="text" class="form-control" id="genre" placeholder="Historia-Terror-Ciencia" name="genre" value="{{old('genre',$book->genre)}}">
            @error('genre')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
        <div>
            <div><label for="floatingInput">Resumen</label></div>
            <textarea class="form-control" placeholder="Escribe una reseña del libro" id="summary" name="summary" style="height: 100px">{{old('summary',$book->summary)}}</textarea>
            @error('summary')
            <small class="alert-danger">{{$message}}</small>
            @enderror
        </div>
      </div>
      @if ($user === $book->owner)
      <button class="btn btn-primary mt-3" type="submit">Actualizar</button>
      @endif
    </form>
@endsection