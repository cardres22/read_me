@extends('layouts.plantilla')
@section('title','book '.$book->bookname)
@section('page','Propietario: '.$book->owner)

@section('content')
<div class="card text-center">
    <div class="card-header">
      <ul class="nav nav-pills card-header-pills">
       @if ($user===$book->owner)
       <li class="nav-item">
        <a class="nav-link bg-warning text-dark m-2" href="{{route('books.edit', $book->id)}}">Editar</a>
      </li>
      <li class="nav-item">
          <form action="{{route('books.delete',$book)}}" class="eliminar" method="POST">
              @csrf
              @method('delete')
              <button type="submit" class="nav-link bg-danger text-light m-2" onclick="return confirm('¿deseas eliminar el libro?')">Eliminar</button>
          </form>
      </li>
       @endif
      </ul>
    </div>
    <div class="card-body" style="background-image: url('{{$book->cover}}'),url('{{$book->cover}}'); background-repeat: no-repeat; background-position: right, left; background-size: 300px 300px;" >
      <h2 class="card-title">{{$book->bookname}}</h2>
      <p class="card-text" style="margin-left: 25%; margin-right: 25%">{{$book->summary}}</p>
      <small style="text-decoration:overline black">Escrito por: {{$book->author}}</small>
      <br>
    <a class="btn btn-success text-light m-2" href="{{route('books.download',$book)}}">Descargar</a>
    </div>
  </div>
   
   
@endsection
