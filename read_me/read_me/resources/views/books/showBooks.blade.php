@extends('layouts.plantilla')
@section('title','show books')
@section('page','Catalogo')

@section('content')
  <div >
    <div class="container">
      <div class="row">
        @foreach ($books as $book)
           
        <div class="card mb-3 mr-3 text-white" style="max-width: 540px; background-color: rgba(0, 0, 0, 0.658)">
            <div class="row g-0">
              <div class="col-md-4">
                <img src="{{$book->cover}}" alt="img" class="mt-3">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title">{{$book->bookname}}</h5>
                  <p class="card-text">{{$book->summary}}</p>
                  <p class="card-text"><small class="text-muted">{{$book->genre}}</small></p>
                  <a class="btn btn-primary m-3" href="{{route('books.select',$book->id)}}" role="button">Mirar libro</a>
                </div>
              </div>
            </div>
          </div>
            
        @endforeach
            </div>
      </div>
  </div>
    {{$books->links()}}
@endsection