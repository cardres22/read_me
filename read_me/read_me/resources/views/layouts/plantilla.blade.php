
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">


    
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>@yield('title')</title>
</head>
<body style="background-image: url('https://i.pinimg.com/originals/f7/0b/92/f70b92730404e934e54ab358c40be3c4.jpg');background-repeat: no-repeat; background-attachment: fixed;">
    <header class="mb-3">
          <nav class="navbar navbar-light navbar-expand-md shadow-sm" style="background-color: rgb(230, 176, 105)">
            <div class="container-fluid">
                <a class="navbar-brand " href='{{route('books.show')}}'><h2 class="fst-italic">Coffe Book</h2>
                  
                </a>
                <h2 class="fst-italic">@yield('page')</h2>
              <div class="d-flex">
                <ul class="navbar-nav mr-auto">
                  <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                  <div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                     Menú
                    </button>
                  <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <li><a class="dropdown-item" href="{{route('books.create')}}">Agregar libro</a></li>
                    <li><a class="dropdown-item" href="{{route('books.show')}}">Lista de libros</a></li>
                    <li><a class="dropdown-item" href="{{route('profile.books')}}">Mis Libros</a></li>
                  </ul>
                </div>
                  </div>
                    <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }}
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                          </form>
                      </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    </header>
    @yield('content')
</body>
</html>