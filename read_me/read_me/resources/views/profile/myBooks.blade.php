@extends('layouts.plantilla')
@section('title','My Books')
@section('page','mis libros')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-3 d-inline-flex">
        <div class="card" style="width: 18rem;">
                @foreach ($user as $profile)
                <img src="{{$profile->img}}" class="card-img-top" alt="...">
            <div class="card-body" style="text-align: center">
                <h2>{{$profile->name}}</h2>
                <h5 class="card-text">{{$profile->email}}</h5>
                <div class="d-grid gap-2">
                    <a class="btn btn-primary" href="#">Editar perfil</a>
                  </div>
                @endforeach
            </div>
          </div>
      </div>
      <div class="col-9">
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th>Titulo</th>
                    <th>Autor</th>
                    <th>Genero</th>
                    <th colspan='2' style="text-align: center">editar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($books as $book)
                    <tr>
                        <td>{{$book->bookname}}</td>
                        <td>{{$book->author}}</td>
                        <td>{{$book->genre}}</td>
                        <td colspan='3' style="text-align: center">
                            <a class="btn btn-warning m-2" href="{{route('books.edit', $book->id)}}">Editar</a>
                            <a class="btn btn-danger m-2 ">Eliminar</a>
                            <a class="btn btn-primary m-2" href="{{route('books.select', $book)}}">Ver</a>
                          </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
      </div>
    </div>
  </div>
@endsection