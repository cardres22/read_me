<?php

namespace Database\Factories;

use App\Models\book;
use Illuminate\Database\Eloquent\Factories\Factory;

class bookFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = book::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bookname'=>$this->faker->sentence(),
            'owner'=>$this->faker->sentence(),
            'author'=>$this->faker->sentence(),
            'cover'=>$this->faker->sentence(),
            'pdf'=>$this->faker->sentence(),
            'genre'=>$this->faker->sentence(),
            'summary'=>$this->faker->paragraph()
        ];
    }
}
