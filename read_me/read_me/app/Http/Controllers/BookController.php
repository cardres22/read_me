<?php

namespace App\Http\Controllers;

use App\Http\Requests\formBook;
use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function showBooks(){
        $books= Book::orderBy('id','desc')->paginate(8);
        return view("books.showBooks",compact('books'));
    }

    public function selectBook($id){

        $book = Book::find($id);
        return view("books.selectBook",[
            'book'=>$book,
            'user'=>Auth::user()->email
        ]);
        //return view("books.selectBook",compact('book'));
    }
    public function createBook(){
        return view("books.createBook");
    }
    public function addBook(formBook $request){

        
        // $book=new Book();
        // $book->bookname = $request->name;
        // $book->owner = $request->owner;
        // $book->author = $request->author;
        // $book->cover = $request->cover;
        // $book->genre = $request->genre;
        // $book->summary = $request->summary;
        // $book->save();

        //$book = Book::create($request->all());
        $request->validate([
            'cover'=>'required|image|max:3000',
            'pdf'=>'required|mimetypes:application/pdf'
        ]);
        $file = $request->file('cover');
        $nameCover=$file->storeAs('public/covers',$file->getClientOriginalName());
        $urlCover = Storage::url($nameCover);
        $file = $request->file('pdf');
        $pdf_name = $file->getClientOriginalName();
        $nameBook = $file->storeAs('public/pdf',$file->getClientOriginalName());
        $urlBook = Storage::url($nameBook);
        $owner =Auth::user()->email;
        $book = Book::create([
            'bookname'=>$request->name,
            'owner'=>$owner,
            'author'=>$request->author,
            'cover'=>$urlCover,
            'pdf'=>$urlBook,
            'pdf_name'=>$pdf_name,
            'genre'=>$request->genre,
            'summary'=>$request->summary
        ]);
        return redirect()->route('books.select',$book->id);


    }

    public function editBook($id){
        $book = Book::find($id);
        $user = Auth::user()->email;
        return view('books.editBook',[
            'book'=>$book,
            'user'=>$user
        ]);
    }
    public function updateBook(formBook $request, Book $book){
        

        // $book->bookname = $request->name;
        // $book->owner = $request->owner;
        // $book->author = $request->author;
        // $book->genre = $request->genre;
        // $book->summary = $request->summary;
        // $book->save();
        if(Auth::user()->email === $book->owner){
            $book->update(
                [
                    'bookname'=>$request->name,
                    'author'=>$request->author,
                    'genre'=>$request->genre,
                    'summary'=>$request->summary
                ]
            );
            return redirect()->route('books.select',$book->id);
        }
        else{
            return "no puedes modificar";
        }
        
    }
    public function deleteBook(Book $book){
        $book->delete();
        return redirect()->route('books.show');
    }

    public function downloadBook(book $book){
        //return response()->download();
       return response()->download('C:/xampp/htdocs/read_me/read_me/read_me/storage/app/public/pdf/'.$book->pdf_name);
       
    }
    public function profileBooks(){
        $book = Book::where('owner',Auth::user()->email)->get();
        return view("books.myBooks",compact('book'));
        //return "hola mundo";
    }
}
