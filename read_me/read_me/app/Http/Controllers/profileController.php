<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class profileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profileBooks(){
        $books = Book::where('owner',Auth::user()->email)->get();
        $user = User::where('email',Auth::user()->email)->get();
    
        return view("profile.myBooks",[
            'books'=>$books,
            'user' =>$user
        ]);
        //return $user;
    }
}
