<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class formBook extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'author'=>'required',
            'genre'=>'required',
            'summary'=>'required'
        ];
    }

    public function attributes()
    {
        return[
            'name'=>'Titulo del libro'
        ];
    }
    public function messages()
    {
        return[
            'summary.required'=>'Debe tener un resumen para que todos conozcan sobre que se trata.'
        ];
    }
}
