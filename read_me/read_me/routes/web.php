<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\profileController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',HomeController::class);
route::get('books', [BookController::class,"showBooks"])->name('books.show');
route::get('books/create', [BookController::class,"createBook"])->name('books.create');
route::get('books/{book}', [BookController::class,"selectBook"])->name('books.select');
Route::post('books/add',[BookController::class,"addBook"])->name('books.add');
Route::get('books/{book}/edit',[BookController::class,"editBook"])->name('books.edit');
Route::put('books/{book}',[BookController::class,"updateBook"])->name('books.update');
Route::delete('books/{book}',[BookController::class,"deleteBook"])->name('books.delete');
Route::get('books/download/{book}',[BookController::class,"downloadBook"])->name('books.download');
route::get('profile/books', [profileController::class,"profileBooks"])->name('profile.books');
//Route::resource('books',bookController::class);
//Route::resource('libros', BookController::class)->parameters('libros'->'book')->names('books');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
